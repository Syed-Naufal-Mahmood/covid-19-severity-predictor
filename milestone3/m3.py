import pandas as pd
import matplotlib.pyplot as plt
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import ConfusionMatrixDisplay, accuracy_score, recall_score, make_scorer, confusion_matrix


class FigNum:
    figNum = 1


def split_data(df):
    Dfdata = df[['age', 'sex', 'latitude', 'longitude', 'date_confirmation', 'Confirmed', 'Deaths', 'Recovered', 'Active', 'Incidence_Rate', 'Case-Fatality_Ratio', 'Survival_Rate']]
    columnOfLabels = df['outcome']
    return train_test_split(Dfdata, columnOfLabels, test_size=0.25, random_state=1)


def validation(classifier, test_data, test_labels, testType, classifierString):
    predictions = classifier.predict(test_data)

    successMargin = accuracy_score(test_labels,predictions)

    labels = ['deceased', 'hospitalized', 'nonhospitalized', 'recovered']
    confMatrix = confusion_matrix(test_labels, predictions)
    disp = ConfusionMatrixDisplay(confusion_matrix=confMatrix, display_labels=labels)
    
    disp.plot()
    plt.figure(FigNum.figNum)
    plt.title(f'{classifierString} {testType} Confusion Matrix ')
    plt.savefig(f'./plots/{classifierString}_{testType}_confusion_matrix.png')
    FigNum.figNum = FigNum.figNum + 1

    print(f'{classifierString} {testType} accuracy : {successMargin}')


if __name__ == "__main__":
    df = pd.read_csv('./dataset/milestone2.csv')

    #split data into train/test
    train_data, test_data, train_labels, test_labels = split_data(df)
    
    #random forest
    rfClassifier = RandomForestClassifier(n_estimators=20)

    #https://stackoverflow.com/questions/55740220/macro-vs-micro-vs-weighted-vs-samples-f1-score
    #https://scikit-learn.org/stable/modules/generated/sklearn.metrics.recall_score.html
    scorers = {
        'accuracy': make_scorer(accuracy_score), 
        'recall': make_scorer(recall_score, average='weighted'),
        'deceased_recall': make_scorer(recall_score, average=None, labels=[0]) #deceased is mapped to value of 0
    }

    #tune hyperparameters
    print('-------------------begin tuning random forest-------------------')
    params = {'n_estimators': [1,10,50,100], 'max_features': [4,6,8,10,12]}
    rfGrid = GridSearchCV(rfClassifier, param_grid=params, scoring=scorers, refit='deceased_recall')
    rfGrid.fit(train_data, train_labels)

    results = pd.DataFrame(rfGrid.cv_results_)
    rfResults = results[['params', 'mean_test_accuracy', 'mean_test_recall', 'mean_test_deceased_recall', 'rank_test_deceased_recall']]
    print(rfResults)
    rfResults.to_csv('./dataset/random_forest.csv')
    print(rfGrid.best_params_)
    print('-------------------random forest tuning complete-------------------')

    
    #adaboost 
    abClassifier = AdaBoostClassifier(
        DecisionTreeClassifier(max_depth=1),
        n_estimators=50
    )
   
    #tune hyperparameters
    print('\n-------------------begin tuning adaboost-------------------')
    params = {'n_estimators': [1,10,50,100], 'learning_rate': [0.5, 1, 1.5, 2]}
    abGrid = GridSearchCV(abClassifier, param_grid=params, scoring=scorers, refit='deceased_recall')
    abGrid.fit(train_data, train_labels)

    results = pd.DataFrame(abGrid.cv_results_)
    abResults = results[['params', 'mean_test_accuracy', 'mean_test_recall', 'mean_test_deceased_recall', 'rank_test_deceased_recall']]
    print(abResults)
    abResults.to_csv('./dataset/adaboost.csv')
    print(abGrid.best_params_)
    print('-------------------adaboost tuning complete-------------------')
    

    #validate on test set
    print('\n-------------------begin validation-------------------')
    validation(rfGrid, test_data, test_labels, 'test data', 'Random Forest')
    validation(abGrid, test_data, test_labels, 'test data', 'AdaBoost')
    print('-------------------validation complete-------------------')
