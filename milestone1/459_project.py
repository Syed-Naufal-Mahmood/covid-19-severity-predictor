import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from collections import Counter
from geopy.geocoders import Nominatim


#Class for storing attributes and the number of missing values
class attribAndNoValMissing:
    def __init__(self, attribute, noMissing):
        self.attribute = attribute
        self.noMissing = noMissing


#Class to track which figure is being output
class FigNum:
    figNum = 1

class countryMissingProvinceTracker:
    def __init__(self, countryName, noOccurences):
        self.countryName = countryName
        self.noOccurences = noOccurences


######################################################################
#Part 1.1
######################################################################


def graphNoOfMissingValues(df, nameOfDataSet):

    #Give all empty cells the text 'Empty'
    columnNames = df.columns 
    
    for item in columnNames:
        df[item].replace(np.nan, 'Empty', inplace=True)
    


    #tinkerdf = df.head(71)
    #print(tinkerdf)
    listOfAttributesAndNoMissing = []

    #For every column and for every item in that column if the value is 'Empty'
    #then add that to the counter, create a attribAndNoValMissing instance for this 
    #column and place the count for that column in there, append that instance to a list
    for item in columnNames:
        Attribute = df[item] 
        counter = 0
        for a in Attribute:
            if a == 'Empty':
                counter = counter + 1
        
        missingValPerAttributeTracker = attribAndNoValMissing(item, counter)
        listOfAttributesAndNoMissing.append(missingValPerAttributeTracker)
        #print(counter)

    print(" ")
    listOfNoMissing = []


    #Output the contents of the list and create a list of just the counts of empty values
    for item in listOfAttributesAndNoMissing:
        print('Attribute:' , item.attribute, ', ', 'No of empty cells:' , item.noMissing)
        listOfNoMissing.append(item.noMissing)


    #Plot the number of empty values v. the names of each column
    plt.figure(FigNum.figNum)
    FigNum.figNum = FigNum.figNum + 1
    axes = plt.subplot()
    axes.bar(columnNames, listOfNoMissing)
    xtickNames = axes.get_xticklabels()
    plt.setp(xtickNames, rotation = 45, horizontalalignment = 'right')
    plt.xlabel('Attributes',fontweight = 'bold')
    plt.ylabel('No. Missing Values ',fontweight = 'bold')
    plt.title('Figure for ' + nameOfDataSet, fontweight = 'bold')
    plt.subplots_adjust(bottom=0.35)
    name = nameOfDataSet[:-4]
    plt.savefig('Figure_for_' + name + '.png')
    plt.close('all')


def plot_lat_long(locationDataDF, individualCasesDF):
    #print(df.columns)
    dfs = [locationDataDF, individualCasesDF]
    k = [['Lat', 'Long_'],
        ['latitude', 'longitude']] #keys for df
    plotName = ['Location Data','Individual Cases']

    for i in range(len(plotName)):
        plt.figure(FigNum.figNum)
        FigNum.figNum = FigNum.figNum + 1
        latitude = dfs[i][k[i][0]] 
        longitude = dfs[i][k[i][1]] 
        plt.scatter(longitude, latitude)
        plt.xlabel('Longitude',fontweight = 'bold')
        plt.ylabel('Latitude',fontweight = 'bold')
        plt.title('Latitude vs Longitude for ' + plotName[i], fontweight = 'bold')
        plt.savefig('Figure_for_' + plotName[i] + '.png')
    
    plt.close('all')


def plot_outcomes(individualCasesDF):
    outcomes = Counter(individualCasesDF['outcome'])
    plt.figure(FigNum.figNum)
    FigNum.fignum = FigNum.figNum + 1
    plt.bar(outcomes.keys(), outcomes.values())
    plt.xlabel('Outcome of COVID Case', fontweight = 'bold')
    plt.ylabel('Number of Cases', fontweight = 'bold')
    plt.title('Number of Outcomes for COVID Cases')
    plt.savefig('Figure_for_Outcomes_for_COVID_Cases.png')
    plt.close('all')


def plot_date_confirmation(individualCasesDF):
    key = 'date_confirmation'
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
    monthBin = [0] * 12 
    for row in individualCasesDF[key]: 
        date = str(row)
        if date != 'nan':
            month = date.split('.')[1]
            month = int(month)
            monthBin[month-1] = monthBin[month-1] + 1

    plt.figure(FigNum.figNum)
    FigNum.fignum = FigNum.figNum + 1
    plt.bar(months, monthBin)
    plt.xlabel('Month', fontweight = 'bold')
    plt.ylabel('Number of Cases', fontweight = 'bold')
    plt.title('Number of COVID Cases per Month')
    plt.savefig('Figure_for_COVID_Cases_per_Month.png')
    plt.close('all')


def plot_sex(individualCasesDF):
    key = 'sex'
    genders = ['Male', 'Female', 'Not Reported']
    genderBin = [0] * 3
    for row in individualCasesDF[key]: 
        gender = str(row)
        if gender.lower() == 'male':
            genderBin[0] = genderBin[0] + 1
        elif gender.lower() == 'female':
            genderBin[1] = genderBin[1] + 1
        else: 
            genderBin[2] = genderBin[2] + 1 

    plt.figure(FigNum.figNum)
    FigNum.fignum = FigNum.figNum + 1
    plt.bar(genders, genderBin)
    plt.xlabel('Gender', fontweight = 'bold')
    plt.ylabel('Number of Cases', fontweight = 'bold')
    plt.title('Number of COVID Cases per gender')
    plt.savefig('Figure_for_COVID_Cases_per_Gender.png')
    plt.close('all')


def aggregate_countries(locationsDF):
    dfAgg = df.groupby('Country_Region').agg({
        'Last_Update': 'max',
        'Lat': 'mean', 
        'Long_': 'mean', 
        'Confirmed': 'sum', 
        'Deaths': 'sum',
        'Recovered': 'sum', 
        'Active': 'sum', 
        'Combined_Key': 'max', #placeholder
        'Incidence_Rate': 'mean', 
        'Case-Fatality_Ratio': 'mean' #placeholder
    }).reset_index()

    return dfAgg


def plot_bar_graph(df, attribx, attriby, xlabel, ylabel, title, imgFile):
    plt.figure(FigNum.figNum)
    FigNum.fignum = FigNum.figNum + 1

    plt.bar(df[attribx], df[attriby])
    plt.xticks(rotation=90)
    plt.xlabel(xlabel, fontweight = 'bold')
    plt.ylabel(ylabel, fontweight = 'bold')
    plt.title(title)
    plt.savefig(imgFile)
    plt.close('all')


######################################################################
#Part 1.2
######################################################################


def cleanTheAgeColumn(df):

    columnNames = df.columns 
    
    #Printout the domain of the age attribute: all the possible values it has in this dataset
    initalValues = df['age'].unique()
    #print("-- Initial Domain of the Age attribute --")
    #print(initalValues)
    
    #This is a list of all the possible ranged ages(no duplicates), i.e, the ones containing a "-""
    entriesWithARangeAge = df[df['age'].str.contains("-")]
    justTheRangedAges = entriesWithARangeAge['age'] 
    #print(justTheRangedAges.unique())
    
    
    #Replacing all the ranged ages with their means
    for item in justTheRangedAges.unique():

        #print('Ranged Age:', item)
        #print(len(item))

        if(len(item) == 6):
            value = int(item[0:2]) + int(item[3:6])
            #print(value)
            df['age'] = df['age'].replace([item], str(value/2))
            #print('My replacement: ', value/2)
        
        elif(item == '18 - 100'):
            value = (18+100)
            df['age'] = df['age'].replace([item], str(value/2))

        elif(len(item) == 5):
            value = int(item[0:2]) + int(item[3:5])
            #print(value)
            df['age'] = df['age'].replace([item], str(value/2))
            #print('My replacement: ', value/2)

        elif(len(item) == 4):
            value = int(item[0]) + int(item[2:4])
            #print(value)
            df['age'] = df['age'].replace([item], str(value/2))
            #print('My replacement: ', value/2)

        elif((len(item) == 3) & (item[1] == '-')):
            value = int(item[0]) + int(item[2])
            df['age'] = df['age'].replace([item], str(value/2))
            #print('My replacement: ', value/2)

        elif((len(item) == 3) & (item[2] == '-')):
            value = item[0:2]
            df['age'] = df['age'].replace([item], value)
            #print('My replacement: ', value)
        else:
            #This case should never be hit, if it is we have a problem
            print('This didnt trigger any ifs', item)
        
    #Handling the age entries with '+'s    
    df['age'] = df['age'].replace(['90+'], '90')
    df['age'] = df['age'].replace(['80+'], '80')
    df['age'] = df['age'].replace(['85+'], '85')


    #Taking a new list from df because now so much of df has been updated
    thePossibleValuesThatAgeHas = df['age'].unique()
    #print(thePossibleValuesThatAgeHas)

    #Handling all the cases where the age was given in months not years
    for item in thePossibleValuesThatAgeHas:
        if('month' in item):
            #print(item)
            value = int(item[0:2])/12
            df['age'] = df['age'].replace([item], value)


    #Requiring yet another list because df has been updated
    checkingValues = df['age'].unique()
    #print(checkingValues)
    
    #Converting all of the ages now to floats such that they can be binned in the future if need be
    #just requires an if tree wherein we replace the age with its range so we could have if age > 0 and 
    #< 10 then replace with 0-10 and so forth
    for item in checkingValues:
        if item != 'Empty':
            df['age'] = df['age'].replace([item], float(item))

    #Outputting the cleaned age domain
    setOfFloats = df['age'].unique()
    #print(" ")
    #print("-- The cleaned age domain --")
    #print(setOfFloats)


#An auxiliary function used during analysing each attribute in 1.2, allowed me to determine the necessity of imputing 
#The province for the processed_location.csv
def verifyingNeedToImputeProvinceForLocationsCSV(df):

    countryMissingProvinceTrackerList = []
    listOfCountriesMissingProvince = []

    for index, row in df.iterrows():
        if(row['Province_State'] == 'Empty'):
            listOfCountriesMissingProvince.append(row['Country_Region'])
        

    for item in listOfCountriesMissingProvince:
        temp = countryMissingProvinceTracker(item, 0)
        countryMissingProvinceTrackerList.append(temp)

    
    for item in countryMissingProvinceTrackerList:   
        for index, row in df.iterrows():
            if(row['Country_Region'] == item.countryName):
                item.noOccurences = item.noOccurences + 1
                
    print('-- Countries Missing Provinces and How many times they occur')
    for item in countryMissingProvinceTrackerList:
        if(item.noOccurences > 2):
            print('Country:', item.countryName, ', Occurences:', item.noOccurences)

            for index, row in df.iterrows():
                if(row['Country_Region'] == item.countryName):
                    print(row)



def imputingProvincesAndCountryIndividualsCSV(df, df2):
    geolocator = Nominatim(user_agent="geoapiExercises")
    print(" ")
    print('Imputing Provinces and Countries, My sincere apologies this will take about 3 minutes')

    #Find the number of countries in the processed_locations.csv that are missing provinces
    listOfCountriesMissingProvinceOK = []
    for index, row in df.iterrows():
        if(row['Province_State'] == 'Empty'):
            listOfCountriesMissingProvinceOK.append(row['Country_Region'])
    

    #For every entry in processed_individuals, if the province is missing and the country is NOT one
    #those listed that it's okay to be missing the province, we impute the province using lat and long    
    for index, row in df2.iterrows():
        
        if((row['country'] == 'Empty') & (row['province'] == 'Taiwan')):
            df2.at[index, 'country'] = 'Taiwan'
        
        if((row['country'] == 'Empty') & (row['province'] == 'Madhya Pradesh')):
            df2.at[index, 'country'] = 'India'
            #print(row)

        if((row['province'] == 'Empty') & (row['country'] not in listOfCountriesMissingProvinceOK)):

            #count = count + 1

            try:
                location = geolocator.reverse(str(row['latitude']) + ',' + str(row['longitude']),timeout=60, language='en', zoom=9)

            except ValueError:
                pass
                #print(str(row['latitude']) + ',' + str(row['longitude']))

            try:
                df2.at[index, 'province'] = location.raw['address']['region']
                
            except KeyError:

                try: 
                    df2.at[index, 'province'] =  location.raw['address']['state']

                except KeyError:

                    try: 
                        df2.at[index, 'province'] = location.raw['address']['province']

                    except KeyError:

                        df2.at[index, 'province'] = location.raw['address']['country']
                        

            #print(row)


def remove_extraneous_columns(df, df2):
    df.drop(columns=['Combined_Key'], inplace=True)
    df2.drop(columns=['additional_information', 'source'], inplace=True)



######################################################################
#Part 1.3
######################################################################


def binning(df, attrib, binRanges: list):
    bins = [0] * (len(binRanges))

    for row in df[attrib]:
        if str(row) != 'Empty' and row != np.nan:
            for i in range((len(binRanges)-1)): 
                if row >= binRanges[i] and row < binRanges[i+1]:
                    bins[i] = bins[i] + 1
        else:
            bins[-1] = bins[-1] + 1
    
    return bins


#TODO: probably should use a histogram, not a bar plot 
def plot_age_bins(df2):
    binRanges = [*range(0,101,10)]
    bins = binning(df2, 'age', binRanges)
    binLabels = ['0-10', '10-20', '20-30', '30-40', '40-50', '50-60', '60-70', '70-80', '80-90', '90-100', 'Empty']
    plt.bar(binLabels[:-1], bins[:-1])
    plt.xlabel('Age Range', fontweight = 'bold')
    plt.ylabel('Number of Cases', fontweight = 'bold')
    plt.title('Number of Cases per Age Range')
    plt.savefig('Figure_for_cases_per_age_range.png')
    plt.close('all')


def plot_country_confirmed_cases_bins(aggregateDF):
    #https://pbpython.com/pandas-qcut-cut.html
    #https://stackoverflow.com/questions/43005462/pandas-bar-plot-with-binned-range/43006004
    equiDepthBin = pd.qcut(aggregateDF['Confirmed'], q=25) 
    equiDepthBin.value_counts(sort=False).plot.bar()
    plt.xlabel('Number of Confirmed Cases', fontweight= 'bold')
    plt.ylabel('Number of Countries in Range')
    plt.xticks(rotation=45)
    plt.title('Number of Countries in a Given Range of Cases EquiDepth')
    plt.savefig('Figure_for_equi_depth_bin_countries_confirmed_cases.png')
    plt.close('all')

    equiWidthBin = pd.cut(aggregateDF['Confirmed'], bins= np.linspace(0,7*10**6, 100)) #regular binning
    equiWidthBin.value_counts(sort=False).plot.bar()
    plt.xlabel('Number of Confirmed Cases', fontweight= 'bold')
    plt.ylabel('Number of Countries in Range')
    plt.title('Number of Countries in a Given Range of Cases EquiWidth')
    plt.savefig('Figure_for_equi_width_bin_countries_confirmed_cases.png')
    plt.close('all')


def handle_outliers(df2):
    dropIndexes = []
    for i in range(len(df2['age'])):
        #print(df2.iloc[i,0])
        if str(df2.iloc[i,0]) != 'Empty' and float(df2.iloc[i,0]) >= 90:
            dropIndexes.append(i)
    df2.drop(df2.index[dropIndexes], inplace=True)
            


######################################################################
#Part 1.4
######################################################################


def aggregate_us_states(df):
    #https://stackoverflow.com/questions/13851535/delete-rows-from-a-pandas-dataframe-based-on-a-conditional-expression-involving
    df.drop(df[df.Province_State == 'Diamond Princess'].index, inplace=True)
    df.drop(df[df.Province_State == 'Grand Princess'].index, inplace=True)
    usRecovered = int(df[df.Province_State == 'Recovered']['Recovered'])
    df.drop(df[df.Province_State == 'Recovered'].index, inplace=True)

    #https://pandas.pydata.org/pandas-docs/version/0.23.1/generated/pandas.core.groupby.DataFrameGroupBy.agg.html 
    #https://stackoverflow.com/questions/31569549/how-to-groupby-a-dataframe-in-pandas-and-keep-columns 
    #aggregate US records by County
    dfUS = df[df['Country_Region'] == 'US'].groupby('Province_State').agg({
        'Country_Region': 'max',
        'Last_Update': 'max',
        'Lat': 'mean', 
        'Long_': 'mean', 
        'Confirmed': 'sum', 
        'Deaths': 'sum',
        'Recovered': 'sum', 
        'Active': 'sum', 
        'Combined_Key': 'max', #placeholder
        'Incidence_Rate': 'mean', 
        'Case-Fatality_Ratio': 'mean' #placeholder
    }).reset_index()

    #remove any US records from dataframe
    df.drop(df[df.Country_Region == 'US'].index, inplace=True)

    #compute necessary values for US aggregates
    dfUS['Case-Fatality_Ratio'] = (dfUS['Deaths']/dfUS['Confirmed'])*100
    dfUS['Recovered'] = int(usRecovered/len(dfUS.index))
    dfUS['Combined_Key'] = dfUS['Province_State'] + ', US'
    
    dfUS.loc[dfUS.Country_Region == 'US', 'Country_Region'] = 'United States'
    newDf = df.append(dfUS, ignore_index=True)
    newDf.sort_values(by=['Country_Region', 'Province_State'], inplace=True)

    return newDf


######################################################################
#Part 1.5
######################################################################

#Joining the two data sets really turned out to be just one line of code
#Below are the attempts to compute the provinces and countries of every entry for the two datasets unfortunately the run time was too long

def reverseEngineerAllLocations(df, df2):
    geolocator = Nominatim(user_agent="geoapiExercises")
    for index, row in df.iterrows():

        try:
            location = geolocator.reverse(str(row['Lat']) + ',' + str(row['Long_']),timeout=60, language='en', zoom=9)
        except (ValueError, TypeError) as e:
            print('Error occured')
            print(row)
            pass

        try:
            df.loc[index, 'Province_State'] = location.raw['address']['region']
        except KeyError:

            try: 
                df.loc[index, 'Province_State'] = location.raw['address']['state']
            except KeyError:

                try: 
                    df.loc[index, 'Province_State'] = location.raw['address']['province']

                except KeyError:

                    try:
                        df.loc[index, 'Province_State'] = location.raw['address']['country']

                    except KeyError:
                        pass

    
        #print(row)


def reverseEngineerAllLocations(row):
    geolocator = Nominatim(user_agent="geoapiExercises")
    try:
        location = geolocator.reverse(str(row['Lat']) + ',' + str(row['Long_']),timeout=60, language='en', zoom=9)

    except (ValueError, TypeError) as e:
        print('Error occured')
        print(row)
        return row['Country_Region']

    try:

        return str(location.raw['address']['region']) + ', ' + str(location.raw['address']['country']) 

    except KeyError:

        try: 
            return str(location.raw['address']['state']) + ', ' + str(location.raw['address']['country']) 

        except KeyError:

            try: 
                return str(location.raw['address']['province']) + ', ' + str(location.raw['address']['country']) 

            except KeyError:

                try:
                    return str(location.raw['address']['country'])

                except KeyError:
                    return row['Country_Region']



## Driver Code starts here ##
if __name__ == "__main__":
    
    df = pd.read_csv('processed_location_Sep20th2020.csv')
    df2 = pd.read_csv('processed_individual_cases_Sep20th2020.csv')

    df['Country_Region'] = df['Country_Region'].replace('Korea, South', 'South Korea')
    df['Country_Region'] = df['Country_Region'].replace('Taiwan*', 'Taiwan')

    listOfCountries = df['Country_Region'].unique()
    print(listOfCountries)

    

    print("Generating and saving charts")

    plot_sex(df2)
    plot_date_confirmation(df2)
    plot_outcomes(df2)
    plot_lat_long(df,df2)

    df = aggregate_us_states(df)
    dfAggCountries = aggregate_countries(df)

    plot_country_confirmed_cases_bins(dfAggCountries)

    plot_bar_graph(dfAggCountries, 'Country_Region', 'Confirmed', 'Countries', 'Number of Confirmed Cases', 'Number of COVID Cases per Country', 'Figure_for_confirmed_cases_per_Country.png')
    plot_bar_graph(dfAggCountries, 'Country_Region', 'Deaths', 'Countries', 'Number of Deaths', 'Number of Deaths Per Country', 'Figure_for_deaths_per_Country.png')
    plot_bar_graph(dfAggCountries, 'Country_Region', 'Recovered', 'Countries', 'Number of Recovered Cases', 'Number of Recovered Cases Per Country', 'Figure_for_recovered_per_Country.png')
    plot_bar_graph(dfAggCountries, 'Country_Region', 'Active', 'Countries', 'Number of Active Cases', 'Number of Cases Per Country', 'Figure_for_active_cases_per_Country.png')
    plot_bar_graph(dfAggCountries, 'Country_Region', 'Incidence_Rate', 'Countries', 'Incidence Rate', 'Incidence Rate Per Country', 'Figure_for_incidence_rate_per_Country.png')
    plot_bar_graph(dfAggCountries, 'Country_Region', 'Case-Fatality_Ratio', 'Countries', 'Case Fatality Ratio', 'Case Fatality Ratio Per Country', 'Figure_for_case_fatality_ratio_per_Country.png')

    print("The statistics for processed_location: ")
    graphNoOfMissingValues(df,'processed_location_Sep20th2020.csv')
    print(" ")

    print("The statistics for processed_individual_cases: ")
    graphNoOfMissingValues(df2,'processed_individual_cases_Sep20th2020.csv')

    cleanTheAgeColumn(df2)
    plot_age_bins(df2)
    handle_outliers(df2)
    remove_extraneous_columns(dfAggCountries,df2)

    imputingProvincesAndCountryIndividualsCSV(df, df2)

    df2['country'] = df2['country'].replace('Czech Republic', 'Czechia')
    


    df_outer = pd.merge(df2, dfAggCountries, left_on= 'country', right_on= 'Country_Region', how='outer')
    #df_outer.drop(columns=['country'], inplace=True)
    df_outer['Survival_Rate'] = (df_outer['Recovered']/df_outer['Confirmed'])*100
    print('The joined cleaned dataset has been saved in Joined.csv')
    
    df_outer.to_csv('./Joined.csv')

    
    


    

#Citations:
#https://www.tutorialgateway.org/python-matplotlib-bar-chart/#:~:text=The%20Python%20matplotlib%20pyplot%20has%20a%20bar%20function%2C,%28x%2C%20height%2C%20width%3D0.8%2C%20bottom%3DNone%2C%20%2A%2C%20align%3D%27center%27%2C%20data%3DNone%2C%20%2A%2Akwargs%29