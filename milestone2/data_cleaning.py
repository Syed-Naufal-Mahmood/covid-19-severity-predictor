import pandas as pd
import numpy as np
from joblib import dump
from random import random
from sklearn.preprocessing import LabelEncoder
from datetime import datetime
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier



def days_between(d2):
    if(d2['date_confirmation'] == 'Empty'):
        return 0
    d1 = datetime.strptime('19.11.2019', "%d.%m.%Y")
    d2['date_confirmation'] = d2['date_confirmation'].split(sep='-')[0].strip()
    d2 = datetime.strptime(d2['date_confirmation'] , "%d.%m.%Y")
    return abs((d2 - d1).days)


def remove_extra_columns(df):
    col0 = df.columns[0]
    df.drop(columns = [col0, 'province', 'Last_Update', 'Country_Region', 'country'], inplace=True)


if __name__ == "__main__":

    #Take in the dataset
    df2 = pd.read_csv('dataset/milestone1.csv')
    
    df = df2.dropna(subset=['sex', 'Country_Region']).reset_index(drop =True)

    #Computing the date_Confirmed as the days since patient zero
    df['date_confirmation'] = df.apply(days_between, axis = 1)


    #Merging the Province and Country Region
    df['Country_Region'] = df['province'] + ', ' +  df['Country_Region']
    df['age'] = df['age'].replace(['Empty'], -1)

    #Encoding the sex column
    df['sex'] = df['sex'].replace([np.NaN], 'Empty')
    encoder = LabelEncoder()
    df['sex'] = encoder.fit_transform(df['sex'])
    listOfSexes = df['sex'].unique()
    

    #Encoding the outcome column
    df['outcome'] = df['outcome'].replace([np.NaN], 'Empty')
    listOfOutcomes = df['outcome'].unique()
    df['outcome'] = encoder.fit_transform(df['outcome'])
    listOfOutcomes = df['outcome'].unique()
    
    

    #Removing extra columns and writing the prepped csv for review later
    remove_extra_columns(df)
    df.to_csv('./dataset/milestone2.csv')

    
