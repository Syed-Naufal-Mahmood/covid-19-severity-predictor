## Execution steps: 
* `datacleaning.py` run this to generate the cleaned `milestone2.csv`
  * Further cleans the data from `milestone1.csv` such that it works with sci-kit learn's classifiers
* `train_classifiers.py` run this to generate saved models as `.pkl` files and the split data as `train_data.csv`, `test_data.csv`, `train_labels.csv`, `test_labels.csv`
  * Trains the Random Forest and AdaBoost classifiers
  * Saves the models 
  * Splits the data into training and test splits and saves them as CSVs
  * Measures time taken to train
  * Tests for overfitting
* `validation.py` run this to get accuracy scores and confusion matrices for each classifier 
  * Loads the trained models
  * Runs classifiers on the training and test data to calculate the accuracy scores 
  * Generates a `.png` for each confusion matrix which is then saved
  * Can be ran directly as long as both models have been saved and all data is present in `./dataset`