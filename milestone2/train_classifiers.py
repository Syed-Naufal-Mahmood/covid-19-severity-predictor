import pandas as pd
import numpy as np
import time
from joblib import dump
from datetime import datetime
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from matplotlib import pyplot as plt


def testingOverfitting(train_data, test_data, train_labels, test_labels):

    listOfAccuracyScoresOnTestDataRF = []
    listOfAccuracyScoresOnTrainingDataRF = []
    listOfAccuracyScoresOnTestDataAB = []
    listOfAccuracyScoresOnTrainingDataAB = []
    listOfMinSamplesALeaf = []
    listOfNoStumps = [] 

    
    for i in range(1,50000,2000):

        print('Testing No of Min Leaves: ', i)

        #Fit classifier under the current conditions  
        rfClassifier = RandomForestClassifier(n_estimators=20, min_samples_leaf= i)
        rfClassifier.fit(train_data, train_labels)


        #Make predictions on the test data
        predictions = rfClassifier.predict(test_data)
        successMargin = accuracy_score(test_labels,predictions)

        #Make predictions on the training data
        predictions_on_training_Data = rfClassifier.predict(train_data)
        successMarginOnTrainingData = accuracy_score(train_labels,predictions_on_training_Data)

        #maintain lists on the accuracy scores of each
        listOfAccuracyScoresOnTestDataRF.append(successMargin)
        listOfAccuracyScoresOnTrainingDataRF.append(successMarginOnTrainingData)

        listOfMinSamplesALeaf.append(i)
    

    for i in range(1,500,50):

        print('Testing no of Stumps', i)

        #Fit the adaboost classifier
        abClassifier = AdaBoostClassifier( DecisionTreeClassifier(max_depth=1), n_estimators = i)
        abClassifier.fit(train_data, train_labels)

        #Make predictions on the test data
        predictions = abClassifier.predict(test_data)
        successMargin = accuracy_score(test_labels,predictions)

        #Make predictions on the training data
        predictions_on_training_Data = abClassifier.predict(train_data)
        successMarginOnTrainingData = accuracy_score(train_labels,predictions_on_training_Data)

        #maintain lists on the accuracy scores of each
        listOfAccuracyScoresOnTestDataAB.append(successMargin)
        listOfAccuracyScoresOnTrainingDataAB.append(successMarginOnTrainingData)

        listOfNoStumps.append(i)
        
        
    #Plot the trend that was found
    plt.plot(listOfMinSamplesALeaf, listOfAccuracyScoresOnTestDataRF, marker = 'o')
    plt.plot(listOfMinSamplesALeaf, listOfAccuracyScoresOnTrainingDataRF, marker = 'o')
    plt.xlabel('Min No Of Samples Per Leaf',fontweight = 'bold')
    plt.ylabel('Accuracy',fontweight = 'bold')
    plt.title('Overfitting Statistics For Random Forest')
    plt.legend(['Test Dataset', 'Training Dataset'])
    plt.savefig('./plots/Overfitting_Random_Forest.png')
    plt.show()
    plt.close('all')
    

    plt.plot(listOfNoStumps, listOfAccuracyScoresOnTestDataAB, marker = 'o')
    plt.plot(listOfNoStumps, listOfAccuracyScoresOnTrainingDataAB, marker = 'o')
    plt.xlabel('No Stumps',fontweight = 'bold')
    plt.ylabel('Accuracy',fontweight = 'bold')
    plt.title('Overfitting Statistics For AdaBoost')
    plt.legend(['Test Dataset', 'Training Dataset'])
    plt.savefig('./plots/Overfitting_AdaBoost.png')
    plt.show()
    plt.close('all')


if __name__ == "__main__":
    df = pd.read_csv('./dataset/milestone2.csv')

    #Splitting the training and Test data
    #Dfdata = df.loc[:, df.columns != 'outcome']
    Dfdata = df[['age', 'sex', 'latitude', 'longitude', 'date_confirmation', 'Confirmed', 'Deaths', 'Recovered', 'Active', 'Incidence_Rate', 'Case-Fatality_Ratio', 'Survival_Rate']]
    columnOfLabels = df['outcome']
    train_data, test_data, train_labels, test_labels = train_test_split(Dfdata, columnOfLabels, test_size=0.2, random_state=1)

    #Producing graphs to check for overfitting
    #testingOverfitting(train_data, test_data, train_labels, test_labels)
    
    rfClassifier = RandomForestClassifier(n_estimators=20)
    start = time.time()
    rfClassifier.fit(train_data, train_labels)
    end = time.time()
    print('random forest classifier time to train:', end-start)
    dump(rfClassifier, './models/random_forest.pkl')


    abClassifier = AdaBoostClassifier(
        DecisionTreeClassifier(max_depth=1),
        n_estimators=50
    )
    start = time.time()
    abClassifier.fit(train_data, train_labels)
    end = time.time()
    print('ada boost classifier time to train:', end-start)
    dump(abClassifier, './models/ada_boost.pkl')

    #save data splits
    train_data.to_csv('./dataset/train_data.csv')
    test_data.to_csv('./dataset/test_data.csv')
    train_labels.to_csv('./dataset/train_labels.csv')
    test_labels.to_csv('./dataset/test_labels.csv')
    