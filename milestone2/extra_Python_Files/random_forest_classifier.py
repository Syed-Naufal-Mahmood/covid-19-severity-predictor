import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
#https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html


def import_data(trainFile, testFile):
    train = pd.read_csv(trainFile)
    test = pd.read_csv(testFile)
    return train, test


if __name__ == "__main__":
    train, test = import_data('./training.csv', './test.csv')
    #joined = pd.read_csv('./joined.csv')

    x_train = train.drop('outcome', axis=1) #joined['age', 'sex', 'province', 'latitude', 'longitude', 'date_confirmation', 'Country_Region', 'Confirmed', 'Deaths', 'Recovered', 'Active', 'Incidence_Rate', 'Case_Fatality_Ratio', 'Survival_Rate']
    x_test = test.drop('outcome', axis=1)
    y_train = train['outcome']
    y_test = test['outcome']


    #print('x train:\n', x_train)
    #print('x test:\n', x_test)
    #print('y train:\n', y_train)
    #print('y test:\n', y_test)

    #clf = RandomForestClassifier()
    #clf.fit(x_train, y_train) 