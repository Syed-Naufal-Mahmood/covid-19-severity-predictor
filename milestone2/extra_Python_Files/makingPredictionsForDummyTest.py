from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.datasets import load_breast_cancer
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import plot_tree # tree diagram

df = pd.read_csv('dummy_test.csv')

columnHeadings = df.columns
lengthOfcolumnheadings = len(columnHeadings)
dfData = df[columnHeadings[0:lengthOfcolumnheadings-1]].reset_index(drop = True)
dfLabels = df[columnHeadings[lengthOfcolumnheadings-1]]


encoder = LabelEncoder()
binary_encoded_labels = pd.Series(encoder.fit_transform(dfLabels))
#print(binary_encoded_labels)

'''
for item in columnHeadings[0:lengthOfcolumnheadings-1]:
    if(item != "weight"):
        dfData[item] = encoder.fit_transform(dfData[item])
'''

#print(pd.get_dummies(dfData, columns=columnHeadings[1:lengthOfcolumnheadings-1]))

updatedDfData = pd.get_dummies(dfData, columns=columnHeadings[1:lengthOfcolumnheadings-1])

train_data, test_data, train_labels, test_labels = train_test_split(updatedDfData, binary_encoded_labels, random_state=1)


classifier = AdaBoostClassifier(
    DecisionTreeClassifier(max_depth=1),
    n_estimators=200
)


classifier.fit(train_data, train_labels)

predictions = classifier.predict(test_data)

print(test_data)

print(predictions)


feature_names = dfData.columns
target_names = df['attractive'].unique().tolist()

plot_tree(classifier, 
          feature_names = feature_names, 
          class_names = target_names, 
          filled = True, 
          rounded = True)

#print(train_data)
#print(train_labels)

'''
print("Pre-Encoding")
print(dfData)

for item in columnHeadings:
    if(item != "attractive"):
        dfData[item] = encoder.fit_transform(dfData[item])

print("Post Encoding")
print(dfData)
'''


