import pandas as pd
import numpy as np
from joblib import dump
from datetime import datetime
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from matplotlib import pyplot as plt
from sklearn.tree import plot_tree




if __name__ == "__main__":

    df = pd.read_csv('./milestone2.csv')


    #Splitting the training and Test data
    Dfdata = df[['age', 'sex', 'latitude', 'longitude', 'date_confirmation', 'Confirmed', 'Deaths', 'Recovered', 'Active', 'Incidence_Rate', 'Case-Fatality_Ratio', 'Survival_Rate']]
    columnOfLabels = df['outcome']
    train_data, test_data, train_labels, test_labels = train_test_split(Dfdata, columnOfLabels, test_size=0.2, random_state=1)


    train_data.to_csv('./train_data.csv')
    test_data.to_csv('./test_data.csv')
    train_labels.to_csv('./train_labels.csv')
    test_labels.to_csv('./test_labels.csv')


    classifier = DecisionTreeClassifier(max_depth=1)
    classifier.fit(train_data, train_labels)
    predictions = classifier.predict(test_data)

    successRate = accuracy_score(test_labels, predictions)
    print(successRate)

    featureNames = Dfdata.columns
    target_names = columnOfLabels.unique().tolist()

    
    lengthOftargetNames = len(target_names)
    for i in range(0,lengthOftargetNames):
        target_names[i] = str(target_names[i])
    
    print(featureNames)
    print(target_names)
    
    plot_tree(classifier, 
        feature_names = featureNames, 
        class_names = target_names, 
        filled = True, 
        rounded = True)

    plt.savefig('tree_visualization.png')
    
    '''
    abClassifier = AdaBoostClassifier( DecisionTreeClassifier(max_depth=1), n_estimators = 50)
    abClassifier.fit(train_data, train_labels)
    predictions = abClassifier.predict(test_data)
    successRate = accuracy_score(test_labels, predictions)
    print(successRate)

    s1 = pd.Series(test_labels, name = 'Actual_Values').reset_index(drop = True)
    s2 = pd.Series(predictions, name = 'Predicted Values')

    results = pd.concat([s1,s2], axis = 1, sort=False)
    results.to_csv('./results.csv')
    '''