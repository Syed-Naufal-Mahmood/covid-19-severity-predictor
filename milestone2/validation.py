import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from joblib import load 
from datetime import datetime
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay


class FigNum:
    figNum = 1


def validation(classifier, test_data, test_labels, testType, classifierString, outputCSV):
    predictions = classifier.predict(test_data)

    successMargin = accuracy_score(test_labels,predictions)

    #s1 = pd.Series(test_labels, name = 'Actual_Values').reset_index(drop = True)
    #s2 = pd.Series(predictions, name = 'Predicted Values')
    #results = pd.concat([s1,s2], axis = 1, sort=False)
    #results.to_csv(outputCSV)

    labels = ['deceased', 'hospitalized', 'nonhospitalized', 'recovered']
    confMatrix = confusion_matrix(test_labels, predictions)
    disp = ConfusionMatrixDisplay(confusion_matrix=confMatrix, display_labels=labels)
    
    disp.plot()
    plt.figure(FigNum.figNum)
    plt.title(f'{classifierString} {testType} Confusion Matrix ')
    plt.savefig(f'./plots/{classifierString}_{testType}_confusion_matrix.png')
    FigNum.figNum = FigNum.figNum + 1

    print(f'{classifierString} {testType} accuracy : {successMargin}')


if __name__ == "__main__":

    test_labels = pd.read_csv('./dataset/test_labels.csv', index_col=0)
    test_data = pd.read_csv('./dataset/test_data.csv', index_col=0)
    train_data = pd.read_csv('./dataset/train_data.csv', index_col=0)
    train_labels = pd.read_csv('./dataset/train_labels.csv', index_col=0)

    #random forest validation
    rfClassifier = load('./models/random_forest.pkl')
    validation(rfClassifier, test_data, test_labels['outcome'], 'test data', 'Random Forest', './random_forest_test_results.csv')
    validation(rfClassifier, train_data, train_labels['outcome'], 'train data', 'Random Forest', './random_forest_train_results.csv')

    #AdaBoost validation
    abClassifier = load('./models/ada_boost.pkl')
    validation(abClassifier, test_data, test_labels['outcome'], 'test data', 'AdaBoost', './ada_boost_test_results.csv')
    validation(abClassifier, train_data, train_labels['outcome'], 'train data', 'AdaBoost', './ada_boost_train_results.csv')

